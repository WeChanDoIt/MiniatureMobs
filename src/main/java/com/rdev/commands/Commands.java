package com.rdev.commands;

import com.rdev.MiniatureMobs;
import com.rdev.configuration.MiniatureMobConfiguration;
import com.rdev.consts.Constants;
import com.rdev.entityai.ZombieMobBaseEntity;
import com.rdev.mob.MobMachine;
import com.rdev.mob.Part;
import com.rdev.mob.PartType;
import com.rdev.utils.ItemBuilder;
import com.rdev.utils.UMaterial;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.util.EulerAngle;
import org.bukkit.util.Vector;

/**
 * A class holding static functions in order to implement the commands from {@link MainCommand}.
 */
public final class Commands {

    /**
     * Command to spawn a mob from the configuration file settings.
     * This command using the nameID of the mob.
     *  @param nameID The name ID of the miniature mob.
     * @param spawnLocation The spawn location of the mob.
     * @param commandSender The command sender of this command.
     * @return Boolean value if the mob has spawned
     */
    public static boolean spawnMob(String nameID, int amount, Location spawnLocation, CommandSender commandSender) {
        MiniatureMobConfiguration mobConfiguration = MiniatureMobs.getInstance().getConfigurationManager().getMobConfigurationByID(nameID);

        if (mobConfiguration == null) {
            commandSender.sendMessage(Constants.Command.MOB_CONFIGUCATION_NOT_FOUND.replace("%mob%", nameID));
            return false;
        }

        Player p = (Player) commandSender;
        MobMachine mobMachine = MiniatureMobs.getInstance().getMobsManager().buildMiniatureMob(mobConfiguration);
        mobMachine.spawn(p.getLocation());
        commandSender.sendMessage(Constants.Command.SPAWN_MOB_SUCCESS.replace("%mob%", nameID));

        return true;
    }

    //just a dev test command
    public static void testCommand(Location location) {

        MobMachine mobMachine = MiniatureMobs.getInstance().getMobsManager().buildMiniatureMob("tiger", new ZombieMobBaseEntity());
        ItemStack headSkull = ItemBuilder.getSkull("eyJ0ZXh0dXJlcyI6eyJTS0lOIjp7InVybCI6Imh0dHA6Ly90ZXh0dXJlcy5taW5lY3JhZnQubmV0L3RleHR1cmUvZmM0MjYzODc0NDkyMmI1ZmNmNjJjZDliZjI3ZWVhYjkxYjJlNzJkNmM3MGU4NmNjNWFhMzg4Mzk5M2U5ZDg0In19fQ==");
        ItemStack bodySkull = ItemBuilder.getSkull("eyJ0ZXh0dXJlcyI6eyJTS0lOIjp7InVybCI6Imh0dHA6Ly90ZXh0dXJlcy5taW5lY3JhZnQubmV0L3RleHR1cmUvOWY0NTdkZWFjMTZkYzNkZmQ1NDdkOGYyZGY5YzA4NzM1Nzc3YmZlNTNiYzg4NDNhMGE2ODFiMmJkMDEyZTY3MyJ9fX0=");
        ItemStack legSkull = ItemBuilder.getSkull("eyJ0ZXh0dXJlcyI6eyJTS0lOIjp7InVybCI6Imh0dHA6Ly90ZXh0dXJlcy5taW5lY3JhZnQubmV0L3RleHR1cmUvOWY0NTdkZWFjMTZkYzNkZmQ1NDdkOGYyZGY5YzA4NzM1Nzc3YmZlNTNiYzg4NDNhMGE2ODFiMmJkMDEyZTY3MyJ9fX0=");
        mobMachine.addPart(new Part(PartType.HEAD, headSkull, false), new Vector(0, -1, 0.84));

        mobMachine.addPart(new Part(PartType.HEAD, bodySkull, true), new Vector(0.2, -0.5, -0.15));
        mobMachine.addPart(new Part(PartType.HEAD, bodySkull, true), new Vector(-0.2, -0.5, -0.15));
        mobMachine.addPart(new Part(PartType.HEAD, bodySkull, true), new Vector(.2, -0.5, -0.58));
        mobMachine.addPart(new Part(PartType.HEAD, bodySkull, true), new Vector(-0.2, -0.5, 0.58));

        mobMachine.addPart(new Part(PartType.HEAD, legSkull, true, new EulerAngle(0f, 15f, 0f)), new Vector(-.35, -0.8, -0.03));
        mobMachine.addPart(new Part(PartType.HEAD, legSkull, true, new EulerAngle(0f, -15f, 0f)), new Vector(.35, -0.8, -0.03));
        mobMachine.addPart(new Part(PartType.HEAD, legSkull, true, new EulerAngle(0f, 15f, 0f)), new Vector(.35, -0.8, -0.73));
        mobMachine.addPart(new Part(PartType.HEAD, legSkull, true,new EulerAngle(0f, -15f, 0f)), new Vector(-.35, -0.8, 0.73));

        mobMachine.spawn(location);
    }

    //memory leak testing
    public static void checkList() {
        Bukkit.broadcastMessage(MiniatureMobs.getInstance().getMobsManager().getMobs().toString());
    }

}
