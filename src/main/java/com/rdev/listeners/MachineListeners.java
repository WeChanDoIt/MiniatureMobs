package com.rdev.listeners;

import com.rdev.MiniatureMobs;
import com.rdev.mob.MobMachine;
import com.rdev.utils.ItemBuilder;
import org.bukkit.entity.LivingEntity;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDeathEvent;
import org.bukkit.inventory.ItemStack;

/**
 * A listener responsible for the mob machine.
 */
public class MachineListeners implements Listener {

    /**
     * Removing mob machine from Lists.
     */
    @EventHandler
    public void MachineDeath(EntityDeathEvent e) {
        MobMachine mobMachine = MiniatureMobs.getInstance().getMobsManager().getMachineByEntity(e.getEntity());
        if (mobMachine == null) return;
        e.getDrops().clear();
        e.getDrops().add(ItemBuilder.getSkull("eyJ0ZXh0dXJlcyI6eyJTS0lOIjp7InVybCI6Imh0dHA6Ly90ZXh0dXJlcy5taW5lY3JhZnQubmV0L3RleHR1cmUvZmM0MjYzODc0NDkyMmI1ZmNmNjJjZDliZjI3ZWVhYjkxYjJlNzJkNmM3MGU4NmNjNWFhMzg4Mzk5M2U5ZDg0In19fQ=="));
        MiniatureMobs.getInstance().getMobsManager().remove(mobMachine);
    }
}
